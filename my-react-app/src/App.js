import React, { Component } from 'react';
import TodoInput from './TodoInput';
import TodoList from './TodoList';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tasks: [
        { title: 'ToDo 1つ目', id: 0 },
        { title: 'ToDo 2つ目', id: 1 },
      ],
      uniqueId: 1,
    };

    // コンテキスト(thisが何を指すのか)を渡すためのおまじない
    // これにより、addTodoメソッドは常にAppコンポーネントのstateを参照できるようになる
    this.addTodo = this.addTodo.bind(this);
    this.resetTodo = this.resetTodo.bind(this);
  }

  addTodo(title) {
    const {
      tasks,
      uniqueId,
    } = this.state;

    tasks.push({
      title,
      id: uniqueId,
    });

    this.setState({
      tasks,
      uniqueId: uniqueId + 1,
    });
  }

  resetTodo() {
    this.setState({
      tasks: [],
    });
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">My ToDo App</h1>
        </header>
        <button onClick={this.resetTodo}>リセット</button>
        <TodoInput addTodo={this.addTodo}/>
        <TodoList tasks={this.state.tasks} />
      </div>
    );
  }
}

export default App;
